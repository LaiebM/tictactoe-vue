// import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.0/dist/vue.esm.browser.js';
"use strict";
    var app = new Vue({
            el: '#app',
            data:   {
                txtArticle:"",
                msg:"Click to start",
                // msg:"Bienvenue dans VueJS, cochez la case pour commencer",
                win:false,
                items: [
                    {
                        name:'Item 1',
                        quantity: 2,
                    }
                ],
                tileID:null,
                currentTurn: 100,
                tiles: [
                    {
                        key: 1,
                        clicked: true,
                        owner:-1,
                    },
                    {
                        key: 2,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 3,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 4,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 5,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 6,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 7,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 8,
                        clicked: false,
                        owner:-1,
                    },
                    {
                        key: 9,
                        clicked: false,
                        owner:-1,
                    },
                ],
                players:[
                    {
                        nom: "Joueur 1",
                        tiles:[]
                    },
                    {
                        nom: "Joueur 2",
                        tiles:[]
                    },
                ],

                winCases:[
                    [1,2,3],
                    [4,5,6],
                    [7,8,9],

                    [1,4,7],
                    [2,5,8],
                    [3,6,9],

                    [1,5,9],
                    [3,5,7],

                ]
            },
            props:{
                checked: {
                    type:Boolean,
                    default: false,
                }
            },
            methods:{
                getTileID(x,y){
                    return ((((y*3)+x)-3));
                },
                resetGame(){
                    console.log("Reset");
                    this.msg="";
                    
                    this.tiles= [
                        {
                            key: 1,
                            clicked: true,
                            owner:-1,
                        },
                        {
                            key: 2,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 3,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 4,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 5,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 6,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 7,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 8,
                            clicked: false,
                            owner:-1,
                        },
                        {
                            key: 9,
                            clicked: false,
                            owner:-1,
                        },
                    ];
                    this.players=[
                        {
                            nom: "Joueur 1",
                            tiles:[]
                        },
                        {
                            nom: "Joueur 2",
                            tiles:[]
                        },
                    ];
                    this.win=false;
                    this.currentTurn=100;

                },

                testWin(){
                    this.players.forEach(player => {
                        let sortedTiles=player.tiles.sort((a,b)=>(a - b));
                        this.winCases.forEach(winCase =>{
                            // console.log(winCase.every(r=>sortedTiles.includes(r)));
                            if (winCase.every(r=>sortedTiles.includes(r))) {
                                console.log(`Le ${player.nom} a gagné !`);
                                this.win=true;
                                this.msg=`Le ${player.nom} a gagné !`;
                            }
                        });

                        // if (sortedTiles.includes(this.winCases)) {
                        //     console.log("includes");
                        // }
                    });
                },

                boxClicked(key){
                    console.log(key);
                    if (this.win)
                    {
                        console.log("La partie est terminée !");
                        this.msg="La partie est terminée !";
                    }
                    else if (this.tiles[key].owner===-1) {
                        this.tiles[key].owner = this.currentTurn;
                        this.players[(this.currentTurn/100)-1].tiles.push(key+1);
                        // console.log('players :>> ', this.players);

                        console.log('owner :>> ', this.tiles[key].owner);
                        switch (this.currentTurn) {
                            case 100:
                                this.currentTurn=200;
                                this.testWin();
                                break;
                        
                            case 200:
                                this.currentTurn=100;
                                this.testWin();
                                break;
                        
                            default:
                                break;
                        }
                        
                    }
                    else{
                        console.log("Cette case appartient déja à un joueur !");
                        this.msg="Cette case appartient déja à un joueur !";
                    }
                },
                printBoxClicked(key){
                    // console.log('key:', key);
                    this.tiles[key].clicked = !this.tiles[key].clicked;
                    // console.log('clicked:', this.tiles[key]);
                },
                // createArticle(){
                //     let article = {
                //         name: this.txtArticle,
                //         quantity: 1,
                //     };
                //     this.items.push(article);
                // },
                incrementItem(index){
                    this.items[index].quantity+=1;
                },
                decreaseItem(index){
                    this.items[index].quantity-=1;
                },
                removeItem(index){
                    this.items.splice(index, 1);
                },
            },
            watch:{
                // currentTurn(newVal,oldVal){
                //     this.msg=`C'est au tour de ${this.players[(newVal/100)-1].nom}`;
                // }
            }
        });